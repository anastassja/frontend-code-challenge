# Desafio Frontend - SkyHub - BIT SP

Para o teste, pedimos que seja entrega em até 5 dias, mas caso precise de mais tempo, nos avise que podemos negociar o prazo.

## Layout

O layout do desafio a ser desenvolvido está em /references/layout

Analogamente, os assets econtram-se em /references/assets

## Desafio
Desenvolver a página do layout, utilizando as seguintes tecnologias:

- HTML 5
- CSS 3
- JavaScript ES6/ES7
- React
- Redux
- Jest

### API
A API a ser consumida pela aplicação está descrita em http://challenge.skyhub.com.br/docs

Para iniciar o desenvolvimento, basta criar uma conta a partir da própria documentação:

- Operação [Nova Conta](http://challenge.skyhub.com.br/docs/#/Contas/accounts_create)
- Clique em "Try it out"
- Preencha seu nome
- E clique em "Execute"
- Use o *token* gerado para realizar as demais operações

#### Obrigatório:
- Código HTML semântico
- Consumir o arquivo os recursos da API disponibilizada
- Design Responsivo
- Testes automatizados

#### Desejável:
- Interações que enriqueçam a navegação pelo layout

Crie um *fork* desse repositório e nos envie um **pull request**.

Não esqueça de ensinar como instalamos e rodamos seu projeto em nosso ambiente. :sunglasses: